export function setDarkMode(darkTheme, doc) {
  const el = doc.querySelector("body");
  if (darkTheme) {
    localStorage.setItem("darkMode", true);
    el.classList.add("theme-dark");
  } else {
    localStorage.setItem("darkMode", false);
    el.classList.remove("theme-dark");
  }
}

export function setDefaultColorMode(doc) {
  setDarkMode(
    localStorage.getItem("darkMode") === null
      ? window.matchMedia &&
          window.matchMedia("(prefers-color-scheme: dark)").matches
      : !!(localStorage.getItem("darkMode") == "true"),
    doc
  );
}

class DarkMode extends HTMLElement {
  constructor() {
    super();

    setDefaultColorMode(document);

    this.onclick = () => {
      setDarkMode(!(localStorage.getItem("darkMode") == "true"), document);
    };
  }
}

export default DarkMode;
